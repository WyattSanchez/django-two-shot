from django.shortcuts import render
from receipts.models import Receipt
from django.contrib.auth.decorators import login_required


@login_required
def show_receipts(request):
    showreceipts = Receipt.objects.filter(purchaser=request.user.id)
    context = {"list": showreceipts}
    return render(request, "receipts/list.html", context)
