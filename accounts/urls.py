from django.urls import path
from accounts.views import Login_view, Logout_view

urlpatterns = [
    path("login/", Login_view, name="login"),
    path("logout/", Logout_view, name="logout"),
]
